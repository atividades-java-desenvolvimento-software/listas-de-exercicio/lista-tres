package br.edu.up.exercicios;
import java.util.Scanner;

import br.edu.up.models.Escola;

public class Exercicio20 {
    public void exercicio20(){
        Scanner sc = new Scanner(System.in);
        Escola escola = new Escola();
        System.out.println("Informe seu nivel como professor:");
        int nivel = sc.nextInt();

        double valorHora = 0;
        switch (nivel) {
            case 1:
                valorHora = 12.0;
                break;
            case 2:
                valorHora = 17.0;
                break;
            case 3:
                valorHora = 25.0;
                break;
            default:
                System.out.println("Nivel invalido");
                break;
        }

        System.out.println("Informe o número de horas de aula: ");
        int horasAula = sc.nextInt();

        double salario = valorHora * horasAula;

        System.out.println("Seu salário como professor: R$" + salario);

        sc.close();
    }
}
