package br.edu.up.exercicios;
import java.util.Scanner;

import br.edu.up.models.GrupoRisco;

public class Exercicio26 {
    public void exercicio26(){
        Scanner sc = new Scanner(System.in);
        GrupoRisco gp = new GrupoRisco();
         System.out.print("Digite o nome do pretendente: ");
         String nome = sc.nextLine();
         System.out.print("Digite a idade do pretendente: ");
         int idade = sc.nextInt();
         System.out.print("Digite o grupo de risco do pretendente (1 - Baixo, 2 - Médio, 3 - Alto): ");
         int grupoRisco = sc.nextInt();
 
         if (idade < 17 || idade > 70) {
             System.out.println("O pretendente não se enquadra na faixa etária necessária para adquirir uma apólice de seguro.");
             sc.close();
             return;
         }
 
         int categoria;
         if (idade >= 17 && idade <= 20) {
             categoria = grupoRisco;
         } else if (idade >= 21 && idade <= 24) {
             categoria = grupoRisco + 1;
         } else if (idade >= 25 && idade <= 34) {
             categoria = grupoRisco + 2;
         } else if (idade >= 35 && idade <= 64) {
             categoria = grupoRisco + 3;
         } else {
             categoria = grupoRisco + 6;
         }
 
         System.out.println("Nome do pretendente: " + nome);
         System.out.println("Idade do pretendente: " + idade);
         System.out.println("Grupo de risco do pretendente: " + grupoRisco);
         System.out.println("Categoria do pretendente: " + categoria);


        sc.close();
    }
}

