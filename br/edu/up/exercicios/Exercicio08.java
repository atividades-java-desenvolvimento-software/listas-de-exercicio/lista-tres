package br.edu.up.exercicios;
import java.util.Scanner;

import br.edu.up.models.AlunoMedia;

public class Exercicio08{
    public void Exercicio08(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Digite o nome do aluno: ");
        String nome = sc.next();

        System.out.println("Informe primeira nota: ");
        double nota1 = sc.nextDouble();
        System.out.println("Informe segunda nota: ");
        double nota2 = sc.nextDouble();
        System.out.println("Informe terceira nota: ");
        double nota3 = sc.nextDouble();

        AlunoMedia al = new AlunoMedia(nome,nota1,nota2,nota3);

        double calcularMedia = al.calcularMedia();

        String mencao = al.obterMencao();

        System.out.println("Nome: " + al.getNome());
        System.out.println("Média: " + calcularMedia);
        System.out.println("Menção: " + mencao);

        sc.close();
    }    

}

