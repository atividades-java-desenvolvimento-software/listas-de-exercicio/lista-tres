package br.edu.up.exercicios;
import java.util.Scanner;

import br.edu.up.models.Abono;

public class Exercicio18 {
    public void exercicio18(){
        Scanner sc = new Scanner(System.in);
        Abono a = new Abono();
        System.out.println("Informe o nome do funcionario: ");
        String nome = sc.nextLine();
        System.out.println("Informe a idade do funcionário: ");
        int idade = sc.nextInt();
        System.out.println("Digite o sexo do funcionario:(M/F) ");
        char sexo = sc.next().charAt(0);
        System.out.println("Digite o salario do funcionario em reais: ");
        double salario = sc.nextDouble();

        double abono = 0;

        if (sexo == 'm' || sexo == 'M'){
            if (idade >= 30) {
                abono = 100;
            }else{
                abono = 50;
            }
            }else if (sexo == 'f' || sexo == 'F') {
                if (idade >= 30) {
                    abono = 200;
                }else{
                    abono = 80;
                }
            }else{
                System.out.println("SEXO INVALIDO");
                System.exit(1);  
            }

            double salarioLiq = salario + abono;

            System.out.println("Funcionário: "+nome);
            System.out.println("Salário Liquido"+salarioLiq);

        sc.close();
    }
}

