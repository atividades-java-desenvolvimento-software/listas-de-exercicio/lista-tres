package br.edu.up.exercicios;
import java.util.Scanner;

import br.edu.up.models.Estudante;

public class Exercicio24 {
    public void exercicio24(){
        Scanner sc = new Scanner(System.in);
        Estudante estudante = new Estudante();
        System.out.print("Digite a nota do trabalho de laboratório (0 a 10): ");
        double notaLaboratorio = sc.nextDouble();
        System.out.print("Digite a nota da avaliação semestral (0 a 10): ");
        double notaAvaliacaoSemestral = sc.nextDouble();
        System.out.print("Digite a nota do exame final (0 a 10): ");
        double notaExameFinal = sc.nextDouble();

        if (notaLaboratorio < 0 || notaLaboratorio > 10 ||
            notaAvaliacaoSemestral < 0 || notaAvaliacaoSemestral > 10 ||
            notaExameFinal < 0 || notaExameFinal > 10) {
            System.out.println("As notas devem estar no intervalo de 0 a 10.");
            sc.close();
            return;
        }

        double pesoLaboratorio = 2;
        double pesoAvaliacaoSemestral = 3;
        double pesoExameFinal = 5;
        double notaFinal = (notaLaboratorio * pesoLaboratorio +
                           notaAvaliacaoSemestral * pesoAvaliacaoSemestral +
                           notaExameFinal * pesoExameFinal) / (pesoLaboratorio + pesoAvaliacaoSemestral + pesoExameFinal);

        System.out.println("A nota final do estudante é: " + notaFinal);

        sc.close();
    }
}

