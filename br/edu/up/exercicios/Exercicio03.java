package br.edu.up.exercicios;
import java.util.Scanner;

public class Exercicio03 {
    public static double salarioVendedor(double salarioFixo, double totalVendas){
        double comissao = totalVendas * 0.15;
        double salarioFinal = salarioFixo + comissao;
        return salarioFinal;
    }
    public void exercicio03(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Informe nome do vendedor: ");
        String nome = sc.next();

        System.out.println("Informe salário fixo: ");
        double salarioFixo = sc.nextDouble();

        System.out.println("Informe o total de vendas no mês: ");
        double totalVendas = sc.nextDouble();

        System.out.println("Salário final: " + salarioVendedor(salarioFixo, totalVendas));

        sc.close();
    }
    
}
