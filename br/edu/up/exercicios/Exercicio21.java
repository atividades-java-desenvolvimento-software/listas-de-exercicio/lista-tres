package br.edu.up.exercicios;
import java.util.Scanner;

import br.edu.up.models.Nadador;

public class Exercicio21 {
    public void exercicio21(){
        Scanner sc = new Scanner(System.in);
        Nadador nadador = new Nadador();
        System.out.println("Digite a idade do nadador: ");
        int idade = sc.nextInt();

        String categoria;
        if (idade >= 5 && idade <= 7) {
            categoria = "Infantil A";
        } else if (idade >= 8 && idade <= 10) {
            categoria = "Infantil B";
        } else if (idade >= 11 && idade <= 13) {
            categoria = "Juvenil A";
        } else if (idade >= 14 && idade <= 17) {
            categoria = "Juvenil B";
        } else if (idade >= 18 && idade <= 25) {
            categoria = "Sênior";
        } else {
            categoria = "Idade fora da faixa etária";
        }

        System.out.println("Categoria do nadador: " + categoria);
        
        sc.close();
    }
    
}
