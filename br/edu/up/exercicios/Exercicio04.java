package br.edu.up.exercicios;
import java.util.Scanner;

import br.edu.up.models.Cotacao;

public class Exercicio04 {
    /*
     * 4.  Elaborar  um  programa  que  efetue  a  apresentação  do  valor  da  conversão  em  real  (R$)  de  um 
    valor  lido  em  dólar  (US$).  O  programa  deverá  solicitar  o  valor  da  cotação  do  dólar  e  também  a 
    quantidade de dólares disponíveis com o usuário  
     */

        public void exercicio04(){
            Scanner sc = new Scanner(System.in);
            System.out.println("Informe a cotação do dolar: ");
            double cotacaoDolar = sc.nextDouble();
            Cotacao cotacao = new Cotacao(cotacaoDolar);
             //nome da classe deveria ser Cotação dolar, mas coloquei Ex4 e criando objeto

            System.out.println("Informe a quantidade de reais que deseja converter: ");
            double valorReal = sc.nextDouble();

            double valorDolar = cotacao.converterParaDolar(valorReal);

            System.out.println("O valor em dolares (US$) é: $" + valorDolar);

            sc.close();
        }



        
     
}
