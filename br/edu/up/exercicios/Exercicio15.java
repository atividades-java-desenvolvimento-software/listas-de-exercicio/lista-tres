package br.edu.up.exercicios;
import java.util.Scanner;

public class Exercicio15 {
    public void exercicio15(){
        Scanner sc = new Scanner(System.in);

        double totalDesc = 0;
        double totalPago = 0;
        double desconto = 0;

        while (true) {
            System.out.println("Digite o tipo de combustivel que deseja: (alcool, diesel ou gasolina) 0 para encerrar: ");
            String combustivel = sc.nextLine().toLowerCase();

            if (combustivel.equals(0)) {
                break;
            }

            System.out.println("Digite o valor do veiculo: ");
            double valorVeiculo = sc.nextDouble();

            switch (combustivel) {
                case "alcool":
                    desconto = 0.25 * valorVeiculo;
                    break;
                case "diesel":
                    desconto = 0.14 * valorVeiculo;
                    break;
                case "gasolina":
                    desconto = 0.21 * valorVeiculo;
                    break;
            
                default:
                    System.out.println("Tipo de combustivel invalido. Tente novamente");
                    break;
            }
            double valorPago = valorVeiculo - desconto;
            totalDesc += desconto;
            totalPago += valorPago;

            System.out.println("Desconto: R$" + desconto);
            System.out.println("Valor a ser pago pelo cliente: R$" + valorPago);

            
            sc.nextLine();
        }

        System.out.println("Total de desconto: R$" + totalDesc);
        System.out.println("Total pago pelos clientes: R$" + totalPago);
        sc.close();
        }

    
    }

