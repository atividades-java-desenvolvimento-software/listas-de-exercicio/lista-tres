package br.edu.up.exercicios;
import java.util.Scanner;

import br.edu.up.models.ContaLuz;

public class Exercicio22 {
    public void exercicio22(){
        Scanner sc = new Scanner(System.in);
        ContaLuz cl = new ContaLuz();
        System.out.println("Digite seu tipo de cliente: 1-residencia - 2-comercio - 3-industria");
        int cliente = sc.nextInt();

        System.out.println("Informe kwh:");
        double qtdKwh = sc.nextDouble();

        double valorKWh;
        switch (cliente) {
            case 1:
                valorKWh = 0.60;
                break;
            case 2:
                valorKWh = 0.48;
                break;
            case 3:
                valorKWh = 1.29;
                break;
            default:
                System.out.println("Tipo de cliente inválido. Digite 1, 2 ou 3.");
                sc.close();
                return;
        }

        double valorConta = qtdKwh * valorKWh;

        System.out.println("VALOR DA CONTA: R$"+valorConta);

        sc.close();
    }


}

