package br.edu.up.exercicios;
import java.util.Scanner;

import br.edu.up.models.PrecoVenda;

public class Exercicio06 {
    /*
     * 6. Faça um programa que receba o preço de custo de um produto e mostre o valor de venda. Sabe-
        se  que  o  preço  de  custo  receberá  um  acréscimo  de  acordo  com  um  percentual  informado  pelo 
        usuário
     */
    
    public static double precoVenda(double custoProduto, double porcentagemInfo){
        return custoProduto + (custoProduto + porcentagemInfo);
    }

    public void exercicio06(){
        Scanner sc = new Scanner(System.in);
        PrecoVenda pc = new PrecoVenda();

        System.out.println("Informe o preço de custo: ");
        double custoProduto= sc.nextDouble();

        System.out.println("Informe o percentual para venda: ");
        double porcentagemInfo = sc.nextDouble();

        System.out.println("Preço de venda: R$" + precoVenda(custoProduto, porcentagemInfo));

        sc.close();
    }

}
