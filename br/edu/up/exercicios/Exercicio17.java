package br.edu.up.exercicios;
import java.util.Scanner;

public class Exercicio17 {
    public void exercicio17(){
        Scanner sc = new Scanner(System.in);
        Funcionario funcionario = new Funcionario();
        
        System.out.print("Digite o nome do funcionário: ");
        String nomeFuncionario = sc.nextLine();

        System.out.print("Digite o salário do funcionário em reais: ");
        double salario = sc.nextDouble();
        System.out.print("Digite o valor do salário mínimo em reais: ");
        double salarioMinimo = sc.nextDouble();

        double novoSalario = salario * (1 + 0.1); // aumento de 10%

        double reajuste = novoSalario - salario;

        System.out.println("Nome do funcionário: " + nomeFuncionario);
        System.out.println("Reajuste: R$" + reajuste);
        System.out.println("Novo salário: R$" + novoSalario);

        double aumentoFolha = novoSalario - salario;

        System.out.println("Aumento na folha de pagamento da empresa: R$" + aumentoFolha);




        sc.close();
    }
    
}
