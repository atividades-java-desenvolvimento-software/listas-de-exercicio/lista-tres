package br.edu.up.exercicios;
import java.util.Scanner;

import br.edu.up.models.Contador;
public class Exercicio09 {

    public void exercicio09(){
        int contador = 0;
        Contador contado = new Contador();
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < 80; i++) {
            System.out.println("Informe um número: ");
            int numero = sc.nextInt();

            if (numero >=10 && numero <= 150) {
                contador++;
            }
        }
        System.out.println("Quantidade de números entre 10 e 150: " + contador);
        sc.close();
    }


}
