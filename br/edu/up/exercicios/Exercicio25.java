package br.edu.up.exercicios;
import java.util.Scanner;

import br.edu.up.models.EstudanteDois;

public class Exercicio25{
    public void exercicio25(){
        Scanner sc = new Scanner(System.in);
             System.out.print("Digite o nome do estudante: ");
        String nome = sc.nextLine();
        System.out.print("Digite o número de matrícula do estudante: ");
        int matricula = sc.nextInt();
        System.out.print("Digite a nota do trabalho de laboratório (0 a 10): ");
        double notaLaboratorio = sc.nextDouble();
        System.out.print("Digite a nota da avaliação semestral (0 a 10): ");
        double notaAvaliacaoSemestral = sc.nextDouble();
        System.out.print("Digite a nota do exame final (0 a 10): ");
        double notaExameFinal = sc.nextDouble();
        EstudanteDois estudanteDois= new EstudanteDois();
    
        if (notaLaboratorio < 0 || notaLaboratorio > 10 ||
            notaAvaliacaoSemestral < 0 || notaAvaliacaoSemestral > 10 ||
            notaExameFinal < 0 || notaExameFinal > 10) {
            System.out.println("As notas devem estar no intervalo de 0 a 10.");
            sc.close();
            return;
        }

        double pesoLaboratorio = 2;
        double pesoAvaliacaoSemestral = 3;
        double pesoExameFinal = 5;
        double notaFinal = (notaLaboratorio * pesoLaboratorio +
                            notaAvaliacaoSemestral * pesoAvaliacaoSemestral +
                            notaExameFinal * pesoExameFinal) / (pesoLaboratorio + pesoAvaliacaoSemestral + pesoExameFinal);

        String classificacao;
        if (notaFinal >= 8 && notaFinal <= 10) {
            classificacao = "A";
        } else if (notaFinal >= 7 && notaFinal < 8) {
            classificacao = "B";
        } else if (notaFinal >= 6 && notaFinal < 7) {
            classificacao = "C";
        } else if (notaFinal >= 5 && notaFinal < 6) {
            classificacao = "D";
        } else {
            classificacao = "R";
        }

        System.out.println("Nome do estudante: " + nome);
        System.out.println("Número de matrícula: " + matricula);
        System.out.println("Nota final: " + notaFinal);
        System.out.println("Classificação: " + classificacao);
        sc.close();
    }
}

