package br.edu.up.exercicios;
import java.util.Scanner;

public class Exercicio16 {
    public void exercicio16(){
        Scanner sc = new Scanner(System.in);

        double salarioMinimo = 1400;

        for (int i = 0; i < 584; i++) {
            System.out.println("Digite o salario do funcionario " + i + " em reais: ");
            double salario = sc.nextDouble();

            double reajuste;
            if(salario < 3 * salarioMinimo){
                reajuste = salario *0.5;
            }else if(salario >= 3 * salarioMinimo && salario <= 10 *salarioMinimo){
                reajuste = salario * 0.2;
            }else if (salario > 10 * salarioMinimo && salario <= 20 * salarioMinimo) {
                reajuste = salario * 0.15; 
            } else {
                reajuste = salario * 0.1; // 10%
            }
            double novoSalario = salario + reajuste;

            System.out.println("Novo salario do funcionario: "+ i + ": R$ " + novoSalario);
        }
        sc.close();
}
}