package br.edu.up.exercicios;
import java.util.Scanner;

import br.edu.up.models.Idade;

public class Exercicio10 {
    public void exercicio10(){
        Scanner sc = new Scanner(System.in);
        Idade idades = new Idade();
        System.out.println("Digite o número de pessoas: ");
        int numPessoas = sc.nextInt();

        for (int i = 0; i < numPessoas; i++) {
            System.out.println("Informe idade da pessoa: " + (i+1));
            int idade = sc.nextInt();

            if (idade >= 18) {
                System.out.println("Maior de idade");
            }else{
                System.out.println("Menor de idade");
            }
        }
        sc.close();
    }
}
