package br.edu.up.exercicios;
import java.util.Scanner;

import br.edu.up.models.Apta;

public class Exercicio13 {
    public void exercicio13(){
        Scanner sc = new Scanner(System.in);

        System.out.println("Digite o número de pessoas: ");
        int pessoas = sc.nextInt();
        Apta apta = new Apta();
        int aptas = 0;
        int naoAptas = 0;

        for (int i = 0; i < pessoas; i++) {
            sc.nextLine();

            System.out.println("Dados da pessoa: " + (i+1) + ":");
            String nome =sc.nextLine();

            System.out.println("Sexo(M/F): ");
            char sexo = sc.nextLine().charAt(0);

            System.out.println("Idade: ");
            int idade = sc.nextInt();

            System.out.println("Saúde (S/N): ");
            char saude = sc.next().charAt(0);

            if ((sexo == 'M' || sexo == 'm') && idade >= 18 && saude == 'S') {
                System.out.println(nome + " está apto para o serviço militar obrigatório.");
                aptas++;
        }else{
            System.out.println(nome + " não está apto para o serviço militar obrigatório.");
            naoAptas++;
        }

       }

       System.out.println("Total de pessoas aptas: " + aptas);
       System.out.println("Total de pessoas não aptas: " + naoAptas);
       sc.close();
    
}
}