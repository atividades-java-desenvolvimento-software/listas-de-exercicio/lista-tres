package br.edu.up.exercicios;
import java.util.Scanner;

import br.edu.up.models.Triangulo;

public class Exercicio19 {
    //verifica se pode formar um triangulo


    public void exercicio19(){
        Scanner sc = new Scanner(System.in);
        Triangulo triangulo = new Triangulo();
        System.out.println("Informe três valores referentes aos lados de um triângulo: ");
        int lado1 = sc.nextInt();
        int lado2 = sc.nextInt();
        int lado3 = sc.nextInt();

        if (triangulo.formaTriangulo(lado1, lado2, lado3)) {
            if (lado1 == lado2 && lado2 == lado3) {
                System.out.println("Triângulo equilatero");
            }else if (lado1 == lado2 || lado1 == lado2 || lado2 == lado3) {
                System.out.println("Triangulo isosceles");
            }else{
                System.out.println("Triangulo escaleno");
            }
        }

        sc.close();
    }
}

