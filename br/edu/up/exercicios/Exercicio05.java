package br.edu.up.exercicios;
import java.util.Scanner;

import br.edu.up.models.Prestacao;

public class Exercicio05 {
    public static double prestacao(double valorCompra){
        double prestacao = valorCompra / 5;
        return prestacao;
    }
    
    public void exercicio05(){
        Scanner sc = new Scanner(System.in);
        Prestacao prestacao = new Prestacao();
        System.out.print("Informe o valor da compra: R$");
        double valorCompra = sc.nextDouble();

        System.out.println("valor de cada prestação é: R$" + prestacao(valorCompra));

        sc.close();
    }
}
