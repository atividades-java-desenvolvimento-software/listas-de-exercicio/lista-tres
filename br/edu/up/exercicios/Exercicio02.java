package br.edu.up.exercicios;
import java.util.Scanner;

import br.edu.up.models.Consumo;

public class Exercicio02 {
    
   

    public void exercicio02(){
        Consumo consumo = new Consumo();
        Scanner sc = new Scanner(System.in);

        System.out.println("Informe a distância percorrida: ");
        double distanciaPercorrida = sc.nextDouble();

        System.out.println("Informe o total de combustivel gasto: ");
        double combustivelGasto = sc.nextDouble();

        System.out.println("Consumo médio: " + consumo.consumoAutomovel(distanciaPercorrida, combustivelGasto));

        sc.close();
    }
}

