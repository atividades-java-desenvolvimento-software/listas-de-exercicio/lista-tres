package br.edu.up.exercicios;
import java.util.Scanner;
import java.util.Objects;

public class Exercicio01 {


    public static double calcularMedia(double nota1, double nota2, double nota3){
        return (nota1 + nota2 + nota3) / 3;
    }
    public void exercicio01(){
        Scanner sc = new Scanner(System.in);

        System.out.println("Informe nome: ");
        String nome = sc.next();

        System.out.println("Informe primeira nota: ");
        double nota1 = sc.nextDouble();
        System.out.println("Informe segunda nota: ");
        double nota2 = sc.nextDouble();
        System.out.println("Informe terceira nota: ");
        double nota3 = sc.nextDouble();

        System.out.println("Média: "+calcularMedia(nota1, nota2, nota3));

        sc.close();
    }
}
