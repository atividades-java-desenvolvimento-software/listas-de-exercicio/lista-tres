package br.edu.up.exercicios;
import java.util.Scanner;

import br.edu.up.models.PesoIdeal;

public class Exercicio23 {
    public void exercicio23(){
        Scanner sc = new Scanner(System.in);
        PesoIdeal peso = new PesoIdeal();
        System.out.print("Digite o nome da pessoa: ");
        String nome = sc.nextLine();
        System.out.print("Digite o sexo da pessoa (M/F): ");
        char sexo = sc.next().charAt(0);
        System.out.print("Digite a altura da pessoa em metros: ");
        double altura = sc.nextDouble();
        System.out.print("Digite a idade da pessoa: ");
        int idade = sc.nextInt();

        double pesoIdeal;
        if (sexo == 'M' || sexo == 'm') {
            if (altura > 1.70) {
                if (idade <= 20)
                    pesoIdeal = (72.7 * altura) - 58;
                else if (idade >= 21 && idade <= 39)
                    pesoIdeal = (72.7 * altura) - 53;
                else
                    pesoIdeal = (72.7 * altura) - 45;
            } else {
                if (idade <= 40)
                    pesoIdeal = (72.7 * altura) - 50;
                else
                    pesoIdeal = (72.7 * altura) - 58;
            }
        } else if (sexo == 'F' || sexo == 'f') {
            if (altura > 1.50) {
                if (idade >= 35)
                    pesoIdeal = (62.1 * altura) - 45;
                else
                    pesoIdeal = (62.1 * altura) - 49;
            } else {
                pesoIdeal = (62.1 * altura) - 44.7;
            }
        } else {
            System.out.println("Sexo inválido. Utilize M ou F.");
            sc.close();
            return;
        }

        System.out.println("Nome: " + nome);
        System.out.println("Peso Ideal: " + pesoIdeal + " kg");

        sc.close();
    }
}

 class Pessoa{
    private String nome;

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public char getSexo() {
        return this.sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public double getAltura() {
        return this.altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public int getIdade() {
        return this.idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }
    private char sexo;
    private double altura;
    private int idade;

    public Pessoa(){

    }
    public Pessoa(String nome,char sexo,double altura,int idade){
        this.nome = nome;
        this.sexo = sexo;
        this.altura = altura;
        this.idade = idade;
    }

}