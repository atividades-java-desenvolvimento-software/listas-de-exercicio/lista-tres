package br.edu.up.exercicios;
import java.util.Scanner;

public class Exercicio12 {
    public void exercicio12(){
        Scanner sc = new Scanner(System.in);
        Veiculo veiculo = new Veiculo();
        int carros2000 =0;
        int carrosAcima2000 =0;

        char continuar;
        do{
            System.out.println("Informe ano do veiculo: ");
            int anoVeiculo = sc.nextInt();

            double desconto;
            if(anoVeiculo <= 2000){
                desconto = 0.12;
                carros2000++;
            }else{
                desconto = 0.07;
            }

            double valorOriginal = 25000;
            double valorDesconto = valorOriginal * desconto;
            double valorPago = valorOriginal - valorDesconto;

            System.out.println("Desconto: R$" + valorDesconto);
            System.out.println("Desconto: R$" + valorPago);

            carrosAcima2000++;

            System.out.println("Deseja continuar calculando desconto? (S/N)");
            continuar = sc.next().charAt(0);
        }while(continuar == 'S' || continuar == 's');

        System.out.println("Total de carros até 2000: " + carros2000);
        System.out.println("Total de carros acima dos anos 2000: " + carrosAcima2000);


        sc.close();
    }
}
